class Project < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_many :tasks
#reference to fields
  validates :name, presence: true, length: { maximum: 50 }
  validates :content, presence: true, length: { maximum: 300 }
  validates :price, presence: true, numericality: { only_interger: true }

  has_attached_file :image, :styles => { :medium => "680x300>", :thumb => "170x75>" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
