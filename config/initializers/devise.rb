Devise.setup do |config|

  config.mailer_sender = 'pandar3n <leonhwh@icloud.com>'
  require 'devise/orm/active_record'
  config.case_insensitive_keys = [ :email ]
  config.strip_whitespace_keys = [ :email ]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 10
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 8..128
  config.reset_password_within = 6.hours
  config.sign_out_via = :delete

  require 'omniauth-google-oauth2'
  config.omniauth :google_oauth2, '1067486497432-gu9ffjkav0upg6ajcceunheieq6qh6fk.apps.googleusercontent.com', 'IiHDZbtgS20t7EHznQCCH218', {access_type: "offline", approval_prompt: ""}

  require 'omniauth-facebook'
  config.omniauth :facebook, '1607658256143930', 'a1ebe10d5c508e5d0eb5fbe2b7e2e373'

  require 'omniauth-github'
  config.omniauth :github, '25e32034255df7a4bd1c', '7a770dca7be4ceed7fb5a781c31dae4386929247', scope: "user:email"
end
